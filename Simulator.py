import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Slider, Button, RadioButtons
import time as tm


fig,ax = plt.subplots()
plt.subplots_adjust(left=0.1, bottom=0.4)

data = plt.axis([0, 100, -100, 100])
axd = plt.gca()
axd.set_ylim(-30,240)
Kpax = plt.axes([0.1, 0.2, 0.65, 0.03])
Kiax = plt.axes([0.1, 0.15, 0.65, 0.03])
Kdax = plt.axes([0.1, 0.1, 0.65, 0.03])
timeax = plt.axes([0.1,0.25, 0.65, 0.03])
setpax = plt.axes([0.1,0.3, 0.65, 0.03])
tuneax = plt.axes([0.8, 0.025, 0.1, 0.04])

thermal_const = .02
h = .01 # heat transfer coefficient
Csys = 7 #2.1 # specific heat
Ctst = .22 # specific heat of thermostream
Csim = .12
dt = .01
ambient = 23
setp = ambient
start_temp = 50
OI = 0
E = setp - start_temp

temp_pts = []
tst_arr = []
pid_arr = []
setp_arr = []
z_arr = []

Kp_tune = 26.25
Ki_tune = 6
Kd_tune = 28.71
temp = start_temp
tst = start_temp

time = np.arange(0, 200, dt)

for pt in time:
    setp_arr.append(setp)
    z_arr.append(225)
    temp_pts.append(ambient)
    pid_arr.append(ambient)
    tst_arr.append(ambient)

lsetp, = axd.plot(time,setp_arr,'--k')
ltemp, = axd.plot(time, temp_pts, 'r', label='system temp')
lpid, = axd.plot(time, pid_arr, 'g', label='PID setpoint')
ltsttemp, = axd.plot(time, tst_arr, 'orange',label='temptronics temp')
axd.legend(bbox_to_anchor=(1.12, 1.25))
#axd.plot(time,z_arr,'--k')
ann = axd.annotate('{:.2f}'.format(temp_pts[len(temp_pts)-1]),(1,.47),xycoords='axes fraction')

Kpsldr = Slider(Kpax, 'Kp', 0, 100, valinit=Kp_tune)
Kisldr = Slider(Kiax, 'Ki', 0, 10, valinit=Ki_tune)
Kdsldr = Slider(Kdax, 'Kd', 0, 100, valinit=Kd_tune)
timesldr = Slider(timeax, 'xlim', 0 ,200, valinit=100)
setpsldr = Slider(setpax, 'Setpoint',-20,225,valinit=30)
button = Button(tuneax, 'Tune')

def tune(event):
    Kpsldr.set_val(Kp_tune)
    Kisldr.set_val(tune_ki())
    Kdsldr.set_val(Kd_tune)
    timesldr.set_val(100)

def update(val):
    Kp = Kpsldr.val
    Ki = Kisldr.val
    Kd = Kdsldr.val
    xlim = timesldr.val
    setp = setpsldr.val

    temp_pts = []
    tst_arr = []
    pid_arr = []
    setp_arr = []
    z_arr = []

    OI = 0
    temp = start_temp
    tst = start_temp
    ctemp = start_temp  # tst coil sim
    E = setp - temp

    for pt in time:
        setp_arr.append(setp)
        z_arr.append(0)

        lastE = E
        E = setp - temp

        ## proportional ##
        OP = E

        ## integral ##
        OI = (OI + E*dt)

        ## derivitive ##
        OD = (E - lastE)/dt

        output = (Kp*OP) + Ki*OI + Kd*OD

        if output>225:
            output=225
        elif output<(-99):
            output=-99

        if pt%1 == 0:
            oset = output   # only update the setpoint for temptronics every second

        dtemp = (oset - ctemp) * Csim
        ctemp += dtemp

        dtemp = (ctemp - tst) * dt*Ctst
        tst += dtemp

        dtemp = (h * (tst - temp))*dt*Csys + (ambient-tst)*dt*Csys*0.002 # *.1 for coefficient
        temp += dtemp

        tst_arr.append(tst)
        temp_pts.append(temp)
        pid_arr.append(output)

    lsetp.set_ydata(setp_arr)
    ltsttemp.set_ydata(tst_arr)
    ltemp.set_ydata(temp_pts)
    lpid.set_ydata(pid_arr)
    axd.set_xlim(0,xlim)
    ann.set_text('{:.2f}'.format(temp_pts[len(temp_pts)-1]))
    fig.canvas.draw_idle()

def tune_ki():
    Kp = Kpsldr.val
    Ki = Ki_tune
    Kd = Kdsldr.val
    setp = setpsldr.val

    delta = 3200
    while (delta > 0.1) | (delta < -0.1):
        temp_pts = []
        tst_arr = []
        pid_arr = []

        OI = 0
        temp = start_temp
        tst = start_temp
        ctemp = start_temp  # tst coil sim
        E = setp - temp

        if delta < -5:
            Ki = Ki - 0.03
        elif delta > 0.1:
            Ki = Ki + 0.001
        elif delta < -0.1:
            Ki = Ki - 0.001

        elif delta > 0:
            Ki = Ki + 0.0001
        elif delta < 0:
            Ki = Ki - 0.0001

        for pt in time:

            lastE = E
            E = setp - temp

            ## proportional ##
            OP = E*dt

            ## integral ##
            OI = (OI + E * dt)

            ## derivitive ##
            OD = (E - lastE) / dt

            output = (Kp * OP) + Ki * OI + Kd * OD
            if output > 225:
                output = 225
            elif output < (-99):
                output = -99

            if pt % 1 == 0:
                oset = output  # only update the setpoint for temptronics every second

            dtemp = (oset - ctemp) * Csim
            ctemp += dtemp

            dtemp = (ctemp - tst) * dt * Ctst
            tst += dtemp

            dtemp = (h * (tst - temp)) * dt * Csys + (ambient - tst) * dt * Csys * 0.001  # *.1 for coefficient
            temp += dtemp

            tst_arr.append(tst)
            temp_pts.append(temp)
            pid_arr.append(output)

        delta = setp - temp_pts[len(temp_pts) - 1]
        print delta
    return Ki


Kpsldr.on_changed(update)
Kisldr.on_changed(update)
Kdsldr.on_changed(update)
timesldr.on_changed(update)
setpsldr.on_changed(update)
button.on_clicked(tune)

plt.show()