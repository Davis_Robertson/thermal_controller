import visa

class TST():
    ## Temptronics TPO4100A Thermostream ##

    def __init__(self,addr):
        # default addr = 'GPIB0::0::INSTR'
        self.rm = visa.ResourceManager()
        self.tst = self.rm.open_resource(addr)

    def get_temp(self):
        ## returns temperature of thermostream ##
        temp = self.tst.read('TMPD?')
        return temp

    def get_DUT_temp(self):
        return

    def heat(self,temp):
        ## set mode to heat (using ulim) ##
        self.tst.write('SETN 0')
        self.tst.write('SETP {}'.format(temp))

    def max_heat(self):
        ## heats at max speed ##
        self.tst.write('SETN 0')

    def cool(self):
        ## set mode to cool (using llim) ##
        self.tst.write('SETN 2')

    def max_cool(self):
        ## cool at max speed ##
        cmd = 'ULIM {}'.format(-99)
        self.tst.write(cmd)
        self.tst.write('SETN 2')

    def ambient(self):
        ## set temperature to ambient temp ##
        self.tst.write('SETN 1')

    def set_temp(self, tempf, tempc):
        ## heat or cool depending on current temperature ##
        if tempc < tempf:
            TST.set_ulim(tempf)
            TST.heat()
        if tempc > tempf:
            TST.set_llim(tempf)
            TST.cool()
        if tempc == tempf:
            TST.set_llim(tempf)
            TST.cool()

    def close(self):
        ## disconnect from device ##
        self.tst.close()