import numpy as np
import Thermostream
import zmq
import time
import sys
import GRU
from multiprocessing import Process


instr = Thermostream.TST('GPIB0::0::INSTR',dsns=1)
socket = None
setp = None
dt = 1

Kpsys = 0
Kisys = 0
Kdsys = 0

Kptst = 0
Kitst = 0
Kdtst = 0

def recv_cmd(port=5555):
    global reset,OI,E
    setp = None
    context = zmq.Context()
    socket = context.socket(zmq.PAIR)
    socket.bind('tcp://*:{}'.format(port))
    Kp_data_tst()
    instr.flow()
    while True:
        try:
            cmd = socket.recv(flags=zmq.NOBLOCK)
            setp = parse_cmd(cmd)
            Esys = 0
            Etst = 0
            OIsys = 0
            OItst = 0
        except Exception:
            pass
        if setp != None:
            sys_set = PID(setp,Kpsys,Kisys,Kdsys,Esys,OIsys,85)
            tst_set = PID(sys_set,Kptst,Kitst,Kdtst,Etst,OItst,225)
            set_temp(tst_set)
        time.sleep(dt)

def parse_cmd(cmd):
    if cmd == 'quit':
        inform = 'Shutting down...'
        socket.send(inform)
        sys.exit()
    else:
        try:
            tempf = float(cmd)
            inform = 'Setting temperature to {} C'.format(tempf)
            print inform
            socket.send(inform)
            return tempf
        except Exception:
            socket.send('Invalid input')

def PID(setp,temp,Kp,Ki,Kd,E,OI,tmax):
    lastE = E
    temp = get_temp()
    E = setp - temp

    ## proportional ##
    OP = E

    ## integral ##
    OI = (OI + E * dt)

    ## derivitive ##
    OD = (E - lastE) / dt

    output = (Kp * OP) + Ki * OI + Kd * OD
    if output > tmax:
        output = tmax
    elif output < (-99):
        output = -99

    return output

def Kp_data_tst():
    Kp_range = np.array(range(5,30,5), dtype=float)
    Kp_range = Kp_range/10.0
    Kp_range = np.hstack((Kp_range, np.array(range(3,60), dtype=float)))
    Kp_range = range(10,60,5)
    for Kp in Kp_range:
        Etst = 0
        OItst = 0

        t_arr = range(100)
        temp_arr = []
        PIDtst_arr = []
        tst_arr = []

        setp = 85
        print Kp
        for t in range(100):
            temp = get_tst_temp()
            tst_set = PID(setp, temp, Kp, 0, 0, Etst, OItst, 225)

            temp_arr.append(temp)
            PIDtst_arr.append(tst_set)
            tst_arr.append(get_tst_temp())

            set_temp(tst_set)
            if t%10 == 0:
                print t
            time.sleep(1)
        while get_tst_temp() < 48:
            set_temp(60)
            time.sleep(1)
        while get_tst_temp() > 52:
            set_temp(0)
            time.sleep(1)


        with open('tst_data/tst-Kp_{}.txt'.format(Kp), 'w+') as f:
            for t in t_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in PIDtst_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in tst_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in temp_arr:
                f.write('{},'.format(t))
            f.write('\n')


def Kd_data_tst(Kp):
    for Kd in range(1, 60, 1):
        Etst = 0
        OItst = 0

        t_arr = range(100)
        temp_arr = []
        PIDtst_arr = []
        tst_arr = []

        setp = 85

        for t in range(100):
            temp = get_tst_temp()
            tst_set = PID(setp, temp, Kp, 0, Kd, Etst, OItst, 225)

            temp_arr.append(get_temp())
            PIDtst_arr.append(tst_set)
            tst_arr.append(get_tst_temp())

            set_temp(tst_set)
            if t%10 == 0:
                print t
            time.sleep(1)

        with open('tst_data/tst-Kd_{}.txt'.format(Kp), 'w+') as f:
            for t in t_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in PIDtst_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in tst_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in temp_arr:
                f.write('{},'.format(t))
            f.write('\n')

def Kp_data():
    Kp_range = np.array(range(2, 30, 2), dtype=float)
    Kp_range = Kp_range / 10.0
    Kp_range = np.hstack((Kp_range, np.array(range(3, 60), dtype=float)))
    for Kp in Kp_range:
        Esys = 0
        Etst = 0
        OIsys = 0
        OItst = 0

        t_arr = range(100)
        temp_arr = []
        PIDtst_arr = []
        PIDsys_arr = []
        tst_arr = []

        setp = 40
        print Kp
        for t in range(100):
            temp = get_tst_temp()
            sys_set = PID(setp, temp, Kp, 0, 0, Esys, OIsys, 85)
            temp = get_temp()
            tst_set = PID(sys_set, temp, Kptst, Kitst, Kdtst, Etst, OItst, 225)

            temp_arr.append(get_temp())
            PIDsys_arr.append(sys_set)
            PIDtst_arr.append(tst_set)
            tst_arr.append(get_tst_temp())

            set_temp(tst_set)
            if t%10 == 0:
                print t
            time.sleep(1)

        with open('sys_data/tune-Kp_{}.txt'.format(Kp),'w+') as f:
            for t in t_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in PIDsys_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in tst_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in temp_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in PIDtst_arr:
                f.write('{},'.format(t))


def Kd_data(Kp):
    for Kd in range(1, 60, 1):
        Esys = 0
        Etst = 0
        OIsys = 0
        OItst = 0

        t_arr = range(100)
        temp_arr = []
        PIDtst_arr = []
        PIDsys_arr = []
        tst_arr = []

        setp = 40

        for t in range(100):
            temp = get_tst_temp()
            sys_set = PID(setp, temp, Kp, 0, Kd, Esys, OIsys, 85)
            temp = get_temp()
            tst_set = PID(sys_set, temp, Kptst, Kitst, Kdtst, Etst, OItst, 225)

            temp_arr.append(get_temp())
            PIDsys_arr.append(sys_set)
            PIDtst_arr.append(tst_set)
            tst_arr.append(get_tst_temp())

            set_temp(tst_set)
            if t%10 == 0:
                print t
            time.sleep(1)

        with open('sys_data/tune-Kd_{}.txt'.format(Kp), 'w+') as f:
            for t in t_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in PIDsys_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in tst_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in temp_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in PIDtst_arr:
                f.write('{},'.format(t))

def set_temp(temp):
    if temp < 25:
        instr.cool(temp)
    else:
        instr.heat(temp)

def get_temp():
    temp = instr.get_DUT_temp()
    return temp

def get_tst_temp():
    temp = instr.get_temp()
    return temp


if __name__ == "__main__":
    mon = Process(target=recv_cmd, args=('5555',))
    gru = Process(target=GRU.GRU, args=('5555',))
    mon.start()