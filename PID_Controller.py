import zmq
import numpy as np
import matplotlib.pyplot as plt
import time
import Thermostream
from multiprocessing import Process
import GRU
import sys
from zmq.eventloop import ioloop, zmqstream
ioloop.install()

socket = None
instr = Thermostream.TST('GPIB0::0::INSTR')
E = 0
OI = 0
reset = 0
dt = 1      # delta time (s)
Kp = 0
Ki = 0
Kd = 0
Kpu = 0
Kdu = 0
Kiu = 0
t = np.arange(0,120,1)
temp_arr = np.zeros((1,120))

def recv_cmd(port=5555):
    global reset,OI,E
    tempf = None
    reset = 0
    context = zmq.Context()
    socket = context.socket(zmq.PAIR)
    socket.bind('tcp://*:{}'.format(port))
    tune_Kp()
    tune_Kd()
    while True:
        try:
            cmd = socket.recv(flags=zmq.NOBLOCK)
            setp = parse_cmd(cmd)
            reset = 0
            E = 0
            OI = 0
        except Exception:
            pass
        if tempf != None:
            PID_control(setp)
        time.sleep(dt)

def parse_cmd(cmd):
    if cmd == 'quit':
        inform = 'Shutting down...'
        socket.send(inform)
        sys.exit()
    else:
        try:
            tempf = float(cmd)
            inform = 'Setting temperature to {} C'.format(tempf)
            print inform
            socket.send(inform)
            return tempf
        except Exception:
            socket.send('Invalid input')


#*********************************************************#
#                                                         #
#                    Main PID Control                     #
#                                                         #
#               Tune to improve efficiency                #
#                                                         #
#*********************************************************#
def PID_control(setp):
    global E,OI
    lastE = E
    temp = get_temp()
    E = setp - temp

    ## proportional ##
    OP = E

    ## integral ##
    OI = (OI + E * dt)

    ## derivitive ##
    OD = (E - lastE) / dt

    output = (Kp * OP) + Ki * OI + Kd * OD
    if output > 225:
        output = 225
    elif output < (-99):
        output = -99

    instr.set_temp(output)


#*********************************************************#
#                                                         #
#               This function tunes Kp                    #
#                                                         #
#       Tuning Kp takes between 5 and 20 minutes          #
#                                                         #
#*********************************************************#
def tune_Kp():
    global Kp,Ki,Kd
    threshold = 0.1     # decrease this value for more precise tuning
    temp_arr = []
    PID_arr = []
    tst_arr = []
    t_arr = []
    setp = 24
    Kp = 20
    t=0
    osc = 0
    while True:
        lmax = []
        lmax_idx = []
        while time<120:             # run each cycle for 120 seconds
            temp = get_temp()
            E = setp - temp         # find error
            output = Kp*E           # calculate proportional output
            if output > 225:        # set max setpoint for temptronics
                output = 225
            elif output < (-99):    # set min setpoint for temptronics
                output = -99
            temp_arr.append(temp)
            tst_arr.append(get_tst_temp())
            PID_arr.appned(output)
            instr.set_temp(output)
            t_arr.append(t)
            t+=1
            time.sleep(dt)

        with open('Kp_{}.txt'.format(Kp),'w+') as f:    # write data to txt file
            for t in t_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in PID_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in tst_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in temp_arr:
                f.write('{},'.format(t))
            f.write('\n')

        for i, val in enumerate(temp_arr):
            if (i != 0) & (i != len(temp_arr) - 1):
                if (val > temp_arr[i - 1]) & (val > temp_arr[i + 1]):   # find local maxima
                    lmax.append(val)                                    # add local maxima values and idxs to array
                    lmax_idx.append(i)
        if len(lmax)>=5:                                                # if there are at least 5 local maximums
            if (abs(lmax[2]-lmax[len(lmax)-1]) < threshold):            # if the difference between local maxima values < threshold
                Ku = Kp                                                 # update ultimate gain
                print 'osc! '
                osc = 1                                                 # this Kp results in oscillation
                Kp -= 0.1                                               # reduce Kp to find minimum Kp with oscillation
            else:
                if osc == 1:                                            # this would be the minimum Kp with oscillation so break loop
                    Ku = Kp
                    print 'BREAK'
                    break
                else:                                                   # increase Kp until the temperature starts oscillating
                    if (abs(lmax[0]-lmax[len(lmax)-1]) > 2):
                        Kp+=5
                    elif (abs(lmax[0]-lmax[len(lmax)-1]) > 1):
                        Kp+=2
                    elif (abs(lmax[0]-lmax[len(lmax)-1]) > 0.5):
                        Kp+=1
        else:
            Kp+=10

    Kp = Ku
    Kd = Kp*1.95


#*********************************************************#
#                                                         #
#               This function tunes Kd                    #
#                                                         #
#       Tuning Kd takes between 5 and 20 minutes          #
#                                                         #
#*********************************************************#
def tune_Kd():
    global Kd
    opt = None
    while True:
        temp_arr = []
        PID_arr = []
        tst_arr = []
        t_arr = []
        E = 0
        setp = 24
        while time < 120:                               # run each cycle for 120 seconds
            temp = get_temp()
            Elast = E
            E = setp - temp                             # find error
            output = (Kp * E) + (Kd * (E-Elast)/dt)     # calculate PD output
            if output > 225:                            # set max setpoint for temptronics
                output = 225
            elif output < (-99):                        # set min setpoint for temptronics
                output = -99
            temp_arr.append(temp)
            tst_arr.append(get_tst_temp())
            PID_arr.appned(output)
            instr.set_temp(output)
            t_arr.append(t)
            t += 1
            time.sleep(dt)
            if (abs(temp_arr[len(temp_arr)-1] - temp_arr[len(temp_arr)-5]) < 0.5) & abs(temp_arr[len(temp_arr)-1] - temp_arr[len(temp_arr)-10]) < 0.5:
            # if the temperature has been the same for 10 seconds, break early
                break

        intersects = []
        for i, t in enumerate(t_arr):
            if (i != 0) & (i != len(t_arr) - 1):
                if (abs(tst_arr[i] - PID_arr[i]) < 0.3) & (abs(tst_arr[i + 1] - PID_arr[i + 1]) > 0.3) & (
                    abs(tst_arr[i - 1] - PID_arr[i - 1]) > 0.3):
                    # if it is an intersect, but lines are not overlaping (not when at temperature)
                    intersects.append(i)
        if intersects > 2:  # if there are at least 3 intersects (too many)
            if opt == 1:  # if there were previously less than 3 intersects, then the transition point has been found
                Kdu = Kd_last
                break
            opt = 0
            Kd_last = Kd
            Kd += 10  # increase Kd in large increments
        else:
            opt = 1
            Kd_last = Kd
            Kd -= 2  # decrease Kd in small increments

        with open('Kd_{}.txt'.format(Kd), 'w+') as f:   # write data to txt file
            for t in t_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in PID_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in tst_arr:
                f.write('{},'.format(t))
            f.write('\n')
            for t in temp_arr:
                f.write('{},'.format(t))
            f.write('\n')


#****************************************************************#
#                                                                #
#                   This function tunes Ki                       #
#                                                                #
#           Tuning Kp takes between 5 and 20 minutes             #
#                                                                #
#****************************************************************#
def tune_Ki():
    E = 0
    for setp in range(-10,120,1):
        temp_arr = []
        PID_arr = []
        tst_arr = []
        t_arr = []
        E = 0
        t=0
        setp = 24
        while True:
            if len(temp_arr)>10:
                if (abs(temp_arr[len(temp_arr)-8:]-temp_arr[len(temp_arr)-1])<0.5).all():
                    print 'pass'
            temp = get_temp()
            Elast = E
            E = setp - temp  # find error
            output = (Kp * E) + (Kd * (E - Elast) / dt)  # calculate PD output
            if output > 225:  # set max setpoint for temptronics
                output = 225
            elif output < (-99):  # set min setpoint for temptronics
                output = -99
            temp_arr.append(temp)
            tst_arr.append(get_tst_temp())
            PID_arr.appned(output)
            instr.set_temp(output)
            t_arr.append(t)
            t += 1
            time.sleep(dt)

def set_temp(temp):
    if temp < 25:
        instr.cool(temp)
    else:
        instr.heat(temp)


def get_temp():
    temp = instr.get_DUT_temp()
    return temp

def get_tst_temp():
    temp = instr.get_temp()
    return temp


if __name__ == "__main__":
    mon = Process(target=recv_cmd, args=('5555',))
    gru = Process(target=GRU.GRU, args=('5555',))
    mon.start()